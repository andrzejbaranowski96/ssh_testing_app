﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project1.Configuration
{
    public class CommandListFile
    {
        private readonly List<string> commandsList;
        public CommandListFile()
        {
            StreamReader fileToRead = new System.IO.StreamReader(FilesTypes.SCRIPT);
            commandsList = ReadFile(fileToRead);

        }

        public static List<string> ReadFile(StreamReader file)
        {
            List<string> ret = new List<string>();
            string line;
            while((line = file.ReadLine()) != null)
            {
                ret.Add(line);
            }

            return ret;
        }

        public List<string> GetCommandsList()
        {
            return commandsList;
        }
    }
}
