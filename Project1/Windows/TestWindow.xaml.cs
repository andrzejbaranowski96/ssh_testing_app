﻿using Project1.Tasks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using Project1.Configuration;
using Renci.SshNet;

namespace Project1.Windows
{
    /// <summary>
    /// Interaction logic for TestWindow.xaml
    /// </summary>
    /// 
    public partial class TestWindow : Window
    {
        public readonly Window parent;
        private readonly RunTests runTests;
        private SshClient connection;

        private void RunTests_PuttingLines(object sender, MyEventArgs e)
        {
            if (!CheckAccess())
            {
                Dispatcher.Invoke(() => { textbox.AppendText(e.Line + Environment.NewLine); textbox.ScrollToEnd(); }); ;
            }
        }

        public TestWindow(Window parent, ref SshClient conn)
        {
            InitializeComponent();
            runTests = new RunTests(this);
            this.parent = parent;
            connection = conn;

            label_connectedTo.Content += $" {(parent as MainWindow).textBox_username.Text}@{(parent as MainWindow).textBox_ip.Text}";
            runTests.PuttingLines += RunTests_PuttingLines;
        }

        private void TestWindow1_Closed(object sender, EventArgs e)
        {
            saveToLog();
            parent.Show();
        }

        private void Button_startstop_Click(object sender, RoutedEventArgs e)
        {
            if (!runTests.commWorker.IsBusy)
            {
                saveToLog();
                CommandListFile cmds = new CommandListFile();
                textbox.Clear();
                button_editScript.IsEnabled = false;
                List<String> configList = cmds.GetCommandsList();
                button_startstop.Content = Application.Current.Resources["TestWindow_Button_Stop"];
                runTests.Communicate(configList, ref  connection);
            }
            else if (runTests.commWorker.IsBusy)
            {
                runTests.CommWorker_Cancel();
                button_editScript.IsEnabled = true;
                button_startstop.Content = Application.Current.Resources["TestWindow_Button_Start"];
            }
        }

        private void Button_editScript_Click(object sender, RoutedEventArgs e)
        {
            EditWindow window = new EditWindow(FilesTypes.SCRIPT);
            window.ShowDialog();
        }

        private void Button_skip_Click(object sender, RoutedEventArgs e)
        {
            runTests.Skip();
        }

        private void saveToLog()
        {
            if ((bool)checkBox_saveOutput.IsChecked && !textbox.Text.Equals(Application.Current.Resources["TestWindow_TextBlock_NewTestPrompt"]))
            {
                DateTime dt = DateTime.Now;
                string filepath = $"logs\\log_{dt.ToString("yyMMdd_HHmmss")}";
                if (File.Exists(filepath))
                {
                    int i = 1;
                    string tmppath = filepath + $"_{i}";
                    while (File.Exists(tmppath))
                    {
                        i++;
                        tmppath = filepath + $"_{i}";
                    }
                    filepath = tmppath;
                }
                filepath += ".log";
                try
                {
                    File.WriteAllLines(filepath, textbox.Text.Split('\n'));
                }
                catch (DirectoryNotFoundException)
                {
                    System.IO.Directory.CreateDirectory("logs");
                    File.WriteAllLines(filepath, textbox.Text.Split('\n'));
                }
            }
        }
    }
}
