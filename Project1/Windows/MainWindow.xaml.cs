﻿using Project1.Windows;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Renci.SshNet;
using System.Net;

namespace Project1
{
    public partial class MainWindow : Window
    {
        public SshClient connection;
        private void TEMP()
        {
            List<string> vls = GetInputValues();

            textBox_ip.Text             = vls[0];
            textBox_username.Text       = vls[1];
            textBox_password.Password   = vls[2];
        }

        public MainWindow()
        {
            InitializeComponent();
            //TEMP();
        }

        private async void Button_connect_Click(object sender, RoutedEventArgs e)
        {
            //TODO: Connection logic
            if (IsValid(textBox_ip) &&
                IsValid(textBox_username) &&
                IsValid(textBox_password))
            {
                SshClient client = new SshClient(textBox_ip.Text, textBox_username.Text, textBox_password.Password);
                label_info.Content = Application.Current.Resources["MainWindow_Info_Connecting"];
                button_connect.IsEnabled = false;
                await Task.Run(() =>
                {
                    try
                    {
                        client.Connect();
                    }
                    catch (Exception ex) {
                        Console.Write(ex.StackTrace);
                    }
                });
                if (client.IsConnected)   //TODO: IF connection successful
                {
                    label_info.Content = Application.Current.Resources["MainWindow_Info_Connected"];
                    connection = client;
                    TestWindow window = new TestWindow(this, ref connection);
                    window.Show();
                    this.Hide();
                    //textBox_password.Password = "";
                    label_info.Content = Application.Current.Resources["MainWindow_Info_NotConnected"];
                }
                else
                {
                    label_info.Content = Application.Current.Resources["MainWindow_Info_CannotConnect"];
                }
            }
            else label_info.Content = Application.Current.Resources["MainWindow_Info_Error"];
            button_connect.IsEnabled = true;
        }


        private bool IsValid(Control control)
        {
            if (control.Name == textBox_ip.Name)
            {
                TextBox pass = control as TextBox;
                if(pass.Text.Equals(""))
                {
                    ChangeBorderStyle(control, ControlState.NOTVALID);
                    return false;
                }
                ChangeBorderStyle(control, ControlState.VALID);
                return true;
            }
            else if (control.Name == textBox_username.Name)
            {
                TextBox username = control as TextBox;
                if (username.Text.Equals(""))
                {
                    ChangeBorderStyle(control, ControlState.NOTVALID);
                    return false;
                }
                ChangeBorderStyle(control, ControlState.VALID);
                return true;
            }
            else if (control.Name == textBox_password.Name)
            {

                return true;
            }
            else
            {
                throw new NotImplementedException("Unknown control");
            }
        }

        private void ChangeBorderStyle(Control control, ControlState state)
        {
            switch (state)
            {
                case ControlState.NOTVALID:
                    control.Background = Brushes.LightPink;
                    break;
                case ControlState.VALID:
                    control.Background = new TextBox().Background;
                    break;
            }
        }

        public static List<string> GetInputValues()
        {
            return new List<string>();// { "192.168.56.101", "root", "Andrzej159" };
        }

    }

    enum ControlState
    {
        VALID, NOTVALID
    }
}
