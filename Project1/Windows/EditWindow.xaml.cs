﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Project1.Configuration;

namespace Project1.Windows
{
    /// <summary>
    /// Interaction logic for EditWindow.xaml
    /// </summary>
    public partial class EditWindow : Window
    {
        readonly string path="";
        public EditWindow(string path)
        {
            InitializeComponent();
            this.path = path;
            LoadFile();
        }

        private void LoadFile()
        {
            try
            {
                textBox.Text = File.ReadAllText(path);
            }
            catch
            {
                File.Create(path);
            }
        }

        private void Save_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                File.WriteAllText(path, textBox.Text);
            }
            catch
            {
                File.Create(path);
                File.WriteAllText(path, textBox.Text);
            }
        }

        private void SaveExit_Button_Click(object sender, RoutedEventArgs e)
        {
            Save_Button_Click(sender, e);
            this.Close();
        }

        private void EditWindow1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (textBox.Text != File.ReadAllText(path))
            {
                if(MessageBox.Show(Application.Current.Resources["EditWindow_Closing_Message"].ToString(),
                                Application.Current.Resources["EditWindow_Closing_PromptTitle"].ToString(),
                                MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    Save_Button_Click(null, null);
                }
            }
        }
    }
}
