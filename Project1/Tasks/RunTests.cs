﻿using Project1.Windows;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using Renci.SshNet;
using System.Threading.Tasks;
using System.Text;
using System.IO;

namespace Project1.Tasks
{
    class RunTests
    {
        public delegate void PutLine(object source, MyEventArgs e);
        public event PutLine PuttingLines;
        public BackgroundWorker commWorker = new BackgroundWorker();
        List<String> configList = null;
        readonly Window parent;
        SshClient client;
        string prompt = " >> ";
        private bool skipRequest = false;

        public RunTests(Window parent)
        {
            this.parent = parent;
        }

        public void Skip()
        {
            skipRequest = true;
        }

        private void SendToSSH(string line, out string response)
        {
            if (!client.IsConnected)
                throw new Renci.SshNet.Common.SshConnectionException($"{prompt}Connection lost or not established.");
            if (line.Equals(""))
                line = '\x0D'.ToString();
            Task<SshCommand> task = new Task<SshCommand>(()=> {
                SendLine($"{prompt}Executing: " + line);
                SshCommand cmd = client.CreateCommand(line);
                cmd.CommandTimeout = TimeSpan.FromSeconds(60);
                try
                {
                    cmd.Execute();
                    return cmd;
                } catch (Renci.SshNet.Common.SshOperationTimeoutException)
                {
                    return null;
                }
            });
            task.Start();
            while (!task.IsCompleted && !commWorker.CancellationPending && !skipRequest) {}
            if (task.IsCompleted)
            {
                if (task.Result != null)
                {
                    //response = task.Result.Result;
                    var rdr = new StreamReader(task.Result.ExtendedOutputStream);
                    string tempstr = task.Result.Result;
                    while (!rdr.EndOfStream)
                    {
                        tempstr += rdr.ReadLine()+Environment.NewLine;
                    }
                    response = tempstr;
                }
                else response = $"{prompt}Execution of command '{line}' timed out.";
                task.Dispose();
            }
            else
            {
                response = "";
            }
            
        }

        public void Communicate(List<String> configList, ref SshClient sshClient)
        {
            this.configList = configList;
            (parent as TestWindow).commProgressBar.Value = 0;
            client = sshClient;

            InitWorker();
            commWorker.RunWorkerAsync();
        }

        private void InitWorker()
        {
            commWorker.DoWork += CommWorker_DoWork;
            commWorker.ProgressChanged += CommWorker_ProgressChanged;
            commWorker.RunWorkerCompleted += CommWorker_RunWorkerCompleted;
            commWorker.WorkerReportsProgress = true;
            commWorker.WorkerSupportsCancellation = true;
        }

        private void DissolveWorker()
        {
            commWorker.DoWork -= CommWorker_DoWork;
            commWorker.ProgressChanged -= CommWorker_ProgressChanged;
            commWorker.RunWorkerCompleted -= CommWorker_RunWorkerCompleted;
            (parent as TestWindow).button_startstop.Content = Application.Current.Resources["TestWindow_Button_Start"];
            (parent as TestWindow).button_editScript.IsEnabled = true;
        }

        private void CommWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            int i = 0;
            
            foreach (String line in configList)
            {
                if (commWorker.CancellationPending)
                {
                    
                    e.Cancel = true;
                    SendLine($"{prompt}Cancelled script execution...");
                    return;
                }
                SendToSSH(line, out string response);
                SendLine(response);

                if(!commWorker.CancellationPending) i++;
                if(skipRequest) { SendLine($"{prompt}Skipped command execution..."+Environment.NewLine); skipRequest = false; }

                commWorker.ReportProgress((int)((((double)i / (double)configList.Count)*100)));
            }
            if (!commWorker.CancellationPending) SendLine($"{prompt}Script executed successfully");
        }

        private void CommWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            (parent as TestWindow).commProgressBar.Value = e.ProgressPercentage;
        }

        private void CommWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            DissolveWorker();   // End of work
        }

        public void CommWorker_Cancel()
        {
            if (commWorker.IsBusy)
            {
                commWorker.CancelAsync();
            }
        }

        private void SendLine(string line)
        {
            //Send a line from list
            PuttingLines?.Invoke(this, new MyEventArgs(line));
        }
    }

    public class MyEventArgs : EventArgs
    {
        public string Line { get; }
        public MyEventArgs(string line)
        {
            this.Line = line;
        }
    }
}
