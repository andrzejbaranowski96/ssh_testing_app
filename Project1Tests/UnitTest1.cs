using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System;
using Project1;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        [Test]
        public void ReadFile_CorrectFile_ReturnsTrue()
        {
            string filePath = System.IO.Path.GetTempFileName();
            List<string> list = new List<string>();
            string filestring = string.Empty;
            Random generator = new Random();
            for (int i = 0; i < 100; i++)
            {
                int number = generator.Next(65535);
                list.Add(number.ToString());
                filestring += number.ToString() + Environment.NewLine;
            }

            File.WriteAllText(filePath, filestring);
            FileStream f = File.OpenRead(filePath);


            List<string> result = Project1.Configuration.CommandListFile.ReadFile(new StreamReader(f));

            Assert.AreEqual(list, result);
        }
    }
}